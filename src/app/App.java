package app;

import java.util.Timer;
import java.util.TimerTask;

import gui.HlavniOknoSeznamove;
import gui.HlavniOknoTabulkove;
import gui.VyberRezimu;

public class App {
	// Statick� metoda, kter� vytvo�� novou instanci hlavn�ho okna.
	public static void main(String[] args) {
		VyberRezimu vyber = new VyberRezimu();
		
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				if(vyber.isVybranRezim()==true) {
					if(vyber.getRezim()==1) {
						HlavniOknoTabulkove okno = new HlavniOknoTabulkove();
						vyber.dispose();
						this.cancel();
					}
					if(vyber.getRezim()==2) {
						HlavniOknoSeznamove seznam = new HlavniOknoSeznamove();
						vyber.dispose();
						this.cancel();
					}
				}
				
			}
		};
		Timer timer = new Timer();
		timer.schedule(task,0, 10);
	}
	
}
