package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneLayout;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import model.Polozka;

public class HlavniOknoSeznamove extends JFrame{
	private List<Polozka>polozky;
	private VyberSouboru vyberSouboru;
	
	
	/**
	 * 
	 */
	public HlavniOknoSeznamove() {
		super();
		setTitle("Evidence");
		setSize(800, 600);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		polozky=new ArrayList<Polozka>();
		this.vyberSouboru=vyberSouboru;
		
		vyberSouboru = new VyberSouboru();
		
		//Definice severu - seznamu
		JScrollPane sever = new JScrollPane();
		DefaultListModel model = new DefaultListModel();
		JList list = new JList(model);
		sever.setLayout(new ScrollPaneLayout());
		sever.setViewportView(list);
		
		
		// Definice ji�n� ��sti s textov�mi poli pro vstup
		JPanel stred = new JPanel();
		stred.setLayout(new BoxLayout(stred, BoxLayout.Y_AXIS));
		
		//Definice ��sti pro textov� pole s popiskami
		JPanel vkladUdaju = new JPanel();
		vkladUdaju.setLayout(new BoxLayout(vkladUdaju,BoxLayout.LINE_AXIS));

		JTextField zadaniNazvu = new JTextField();
		JTextField zadaniCeny = new JTextField();
		JTextField zadaniMeny = new JTextField();
		zadaniNazvu.setFont(new Font("Arial", Font.PLAIN, 12));
		;
		zadaniCeny.setFont(new Font("Arial", Font.PLAIN, 12));
		zadaniMeny.setFont(new Font("Arial", Font.PLAIN, 12));
		JLabel popisZadaniNazvu = new JLabel("N�zev:");
		popisZadaniNazvu.setFont(new Font("Arial", Font.BOLD, 12));
		JLabel popisZadaniCeny = new JLabel("Cena:");
		popisZadaniCeny.setFont(new Font("Arial", Font.BOLD, 12));
		JLabel popisZadaniMeny = new JLabel("M�na:");
		popisZadaniMeny.setFont(new Font("Arial", Font.BOLD, 12));

		vkladUdaju.add(popisZadaniNazvu);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(zadaniNazvu);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(popisZadaniCeny);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(zadaniCeny);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(popisZadaniMeny);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(zadaniMeny);
		vkladUdaju.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		stred.add(vkladUdaju);
		vkladUdaju.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

					
		
		// Definice panelu v ji�n� ��sti pro tla��tka pro p�id�n�, �pravy a smaz�n� �daj�.
		JPanel tlacitkaUdaju = new JPanel();
		tlacitkaUdaju.setLayout(new BoxLayout(tlacitkaUdaju, BoxLayout.LINE_AXIS));

		JButton pridat = new JButton("P�idat");
		pridat.setFont(new Font("Arial", Font.BOLD, 12));

		pridat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					String cenaPrevod = zadaniCeny.getText();
				    final double cena = Integer.valueOf(cenaPrevod);
					polozky.add(new Polozka(zadaniNazvu.getText(), cena, zadaniMeny.getText()));
				model.addElement(polozky.get(polozky.size()-1));
			}
		});

		JButton upravit = new JButton("Upravit");
		upravit.setFont(new Font("Arial", Font.BOLD, 12));
		
		/*upravit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tabulka.getSelectedRow() != -1) {
					double cena = 0;
					String cenaPrevod = zadaniCeny.getText();
					if (zadaniCeny.getText().length() > 0) {
						cena = Integer.valueOf(cenaPrevod);
						}
					if (zadaniNazvu.getText().length() > 0) {
						model.setValueAt(zadaniNazvu.getText(), tabulka.getSelectedRow(), 0);
						polozky.get(tabulka.getSelectedRow()).setNazev(zadaniNazvu.getText());
						System.out.println(polozky.get(tabulka.getSelectedRow()).toString());
					}
					if (zadaniCeny.getText().length() > 0) {
						model.setValueAt(cena, tabulka.getSelectedRow(), 1);
						polozky.get(tabulka.getSelectedRow()).setCena(cena);
						System.out.println(polozky.get(tabulka.getSelectedRow()).toString());
					}
					if (zadaniMeny.getText().length() > 0) {
						model.setValueAt(zadaniMeny.getText(), tabulka.getSelectedRow(), 2);
						polozky.get(tabulka.getSelectedRow()).setMena(zadaniMeny.getText());
						System.out.println(polozky.get(tabulka.getSelectedRow()).toString());
					}
					zadaniNazvu.setText("");
					zadaniCeny.setText("");
					zadaniMeny.setText("");
				} else {
					zobrazChybu("Nevybr�n ��dek","Nejd��ve pros�m vyberte ��dek, kter� chcete upravit.");
				}

			}

		});*/

			
		

		JButton smazat = new JButton("Smazat");
		smazat.setFont(new Font("Arial", Font.BOLD, 12));

		smazat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (list.getSelectedIndex()!= -1) {
					polozky.remove(list.getSelectedIndex());
					model.remove(list.getSelectedIndex());
				} else {
					zobrazChybu("Nevybr�n ��d�k", "Nejd��ve pros�m vyberte ��dek, kter� chcete vymazat.");
				}
			}
		});
				
				
		JButton secist = new JButton("Se��st");
		secist.setFont(new Font("Arial", Font.BOLD, 12));
		
		
		
		tlacitkaUdaju.add(pridat);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.add(upravit);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.add(smazat);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.add(secist);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.setBorder(BorderFactory.createEmptyBorder(0, 60, 10, 0));
		stred.add(tlacitkaUdaju);
		
		
		
		
		add(sever,BorderLayout.NORTH);
		add(stred, BorderLayout.SOUTH);
		pack();
		setVisible(true);

	}
	public void zobrazChybu(final String nadpis ,final String zprava) {
		JOptionPane chyba = new JOptionPane();
		chyba.showMessageDialog(this, zprava,nadpis, chyba.ERROR_MESSAGE);
	}
	public double sectiCeny() {
	    double vysledek=0;
		for(Polozka polozka:polozky) {
		vysledek+=polozka.getCena();	
		}
		return vysledek;
	}
		
		
	}
	



