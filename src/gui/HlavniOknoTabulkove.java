package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneLayout;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import model.Polozka;

public class HlavniOknoTabulkove extends JFrame {
	
	private List<Polozka>polozky;
	private VyberSouboru vyberSouboru;
	
	public HlavniOknoTabulkove() {
		// Definice hlavn�ho okna.
		super();
		setTitle("Evidence");
		setSize(800, 600);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		polozky=new ArrayList<Polozka>();
		this.vyberSouboru=vyberSouboru;
		
		vyberSouboru = new VyberSouboru();
		
		// Definice severu - horn�ho panelu tla��tek.
		
		JPanel sever = new JPanel();
		sever.setLayout(new BoxLayout(sever,BoxLayout.LINE_AXIS));
		sever.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		
		JButton novy = new JButton(new ImageIcon("Assets/New.gif"));
		novy.setToolTipText("Nov�");
				
		sever.add(novy);
		
		JButton otevrit = new JButton(new ImageIcon("Assets/Open.gif"));
		otevrit.setToolTipText("Otev��t");
		sever.add(otevrit);
		
		JButton ulozit = new JButton(new ImageIcon("Assets/Save.gif"));
		ulozit.setToolTipText("Ulo�it");
		sever.add(ulozit);
		
		
		
		//Definice st�edu - tabulky
		JScrollPane stred = new JScrollPane();
		DefaultTableModel model = new DefaultTableModel(new Object[] { "N�zev", "Cena", "M�na","Za�krtnut�" }, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return column==3;
			}
			@Override
			public Class<?> getColumnClass(int columnIndex) {
				if(columnIndex==3) {
					return Boolean.class;
				}
				return super.getColumnClass(columnIndex);
			}
		};
		JTable tabulka = new JTable(model);
		tabulka.setFont(new Font("Arial", Font.PLAIN, 12));
		tabulka.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 12));
		tabulka.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		stred.setLayout(new ScrollPaneLayout());
		stred.setViewportView(tabulka);

		// Definice ji�n� ��sti s textov�mi poli pro vstup
		JPanel jih = new JPanel();
		jih.setLayout(new BoxLayout(jih, BoxLayout.Y_AXIS));
		
		//Definice ��sti pro textov� pole s popiskami
		JPanel vkladUdaju = new JPanel();
		vkladUdaju.setLayout(new BoxLayout(vkladUdaju,BoxLayout.LINE_AXIS));

		JTextField zadaniNazvu = new JTextField();
		JTextField zadaniCeny = new JTextField();
		JTextField zadaniMeny = new JTextField();
		zadaniNazvu.setFont(new Font("Arial", Font.PLAIN, 12));
		;
		zadaniCeny.setFont(new Font("Arial", Font.PLAIN, 12));
		zadaniMeny.setFont(new Font("Arial", Font.PLAIN, 12));
		JLabel popisZadaniNazvu = new JLabel("N�zev:");
		popisZadaniNazvu.setFont(new Font("Arial", Font.BOLD, 12));
		JLabel popisZadaniCeny = new JLabel("Cena:");
		popisZadaniCeny.setFont(new Font("Arial", Font.BOLD, 12));
		JLabel popisZadaniMeny = new JLabel("M�na:");
		popisZadaniMeny.setFont(new Font("Arial", Font.BOLD, 12));

		vkladUdaju.add(popisZadaniNazvu);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(zadaniNazvu);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(popisZadaniCeny);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(zadaniCeny);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(popisZadaniMeny);
		vkladUdaju.add(Box.createRigidArea(new Dimension(10, 0)));
		vkladUdaju.add(zadaniMeny);
		vkladUdaju.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		jih.add(vkladUdaju);
		vkladUdaju.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

					
		
		// Definice panelu v ji�n� ��sti pro tla��tka pro p�id�n�, �pravy a smaz�n� �daj�.
		JPanel tlacitkaUdaju = new JPanel();
		tlacitkaUdaju.setLayout(new BoxLayout(tlacitkaUdaju, BoxLayout.LINE_AXIS));

		JButton pridat = new JButton("P�idat");
		pridat.setFont(new Font("Arial", Font.BOLD, 12));

		pridat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String cenaPrevod = zadaniCeny.getText();
			    final	double cena = Integer.valueOf(cenaPrevod);
				polozky.add(new Polozka(zadaniNazvu.getText(), cena, zadaniMeny.getText()));
				model.addRow(new Object[] { zadaniNazvu.getText(), cena, zadaniMeny.getText(),new Boolean(false)});
				zadaniNazvu.setText("");
				zadaniCeny.setText("");
				zadaniMeny.setText("");
			}
		});

		JButton upravit = new JButton("Upravit");
		upravit.setFont(new Font("Arial", Font.BOLD, 12));
		// P�id�n� a definice listeneru pro tla��tko upravit, v�etn� podm�nek, aby se v
		// ��dc�ch tabulky upravovaly jenom hodnoty z vypln�n�ch textov�ch pol�
		upravit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tabulka.getSelectedRow() != -1) {
					double cena = 0;
					String cenaPrevod = zadaniCeny.getText();
					if (zadaniCeny.getText().length() > 0) {
						cena = Integer.valueOf(cenaPrevod);
						}
					if (zadaniNazvu.getText().length() > 0) {
						model.setValueAt(zadaniNazvu.getText(), tabulka.getSelectedRow(), 0);
						polozky.get(tabulka.getSelectedRow()).setNazev(zadaniNazvu.getText());
						System.out.println(polozky.get(tabulka.getSelectedRow()).toString());
					}
					if (zadaniCeny.getText().length() > 0) {
						model.setValueAt(cena, tabulka.getSelectedRow(), 1);
						polozky.get(tabulka.getSelectedRow()).setCena(cena);
						System.out.println(polozky.get(tabulka.getSelectedRow()).toString());
					}
					if (zadaniMeny.getText().length() > 0) {
						model.setValueAt(zadaniMeny.getText(), tabulka.getSelectedRow(), 2);
						polozky.get(tabulka.getSelectedRow()).setMena(zadaniMeny.getText());
						System.out.println(polozky.get(tabulka.getSelectedRow()).toString());
					}
					zadaniNazvu.setText("");
					zadaniCeny.setText("");
					zadaniMeny.setText("");
				} else {
					zobrazChybu("Nevybr�n ��dek","Nejd��ve pros�m vyberte ��dek, kter� chcete upravit.");
				}

			}

		});

		JButton smazat = new JButton("Smazat");
		smazat.setFont(new Font("Arial", Font.BOLD, 12));

		smazat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tabulka.getSelectedRow() != -1) {
					polozky.remove(tabulka.getSelectedRow());
					model.removeRow(tabulka.getSelectedRow());
				} else {
					zobrazChybu("Nevybr�n ��d�k", "Nejd��ve pros�m vyberte ��dek, kter� chcete vymazat.");
				}
			}
		});
		JButton secist = new JButton("Se��st");
		secist.setFont(new Font("Arial", Font.BOLD, 12));
		secist.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			if(!polozky.isEmpty()&&model.getRowCount()>0) {
			double celkovaCena = sectiCeny();
			model.addRow(new Object[] {"Celkem",celkovaCena,"CZ",null});
				
			}else {
				zobrazChybu("��dn� data", "Nejd��ve vlo�te data do tabulky.");
			}
		}
	});
		
		
		tlacitkaUdaju.add(pridat);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.add(upravit);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.add(smazat);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.add(secist);
		tlacitkaUdaju.add(Box.createRigidArea(new Dimension(50, 0)));
		tlacitkaUdaju.setBorder(BorderFactory.createEmptyBorder(0, 60, 10, 0));
		jih.add(tlacitkaUdaju);
		
		//Definice ActionListener� pro tla��tka Nov�, Otev��t a ulo�it
		
		novy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.setRowCount(0);
				polozky.clear();
				
			}
		});
		
		otevrit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
					if(vyberSouboru.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
						model.setRowCount(0);
						polozky.clear();
						String nazevSouboru = vyberSouboru.getSelectedFile().getAbsolutePath(); 
						FileInputStream fos;
						try {
							fos = new FileInputStream(nazevSouboru);
							Scanner scan = new Scanner(fos);
							while(scan.hasNextLine()) {
							String nazev = scan.next();
							String cena = scan.next();
							int cenaPrevod=(int)Double.parseDouble(cena);
							String mena = scan.next();
							polozky.add(new Polozka(nazev,(double)cenaPrevod,mena));
							scan.nextLine();
							}
							scan.close();
							fos.close();
							
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						for(Polozka polozka:polozky) {
							System.out.println(polozka.toString());
							model.addRow(new Object[] {polozka.getNazev(),polozka.getCena(),polozka.getMena(),new Boolean(false)});
							
						}
						
					}
					
				
				}
				
			
			
		});
		
		ulozit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(polozky.isEmpty()==false && model.getRowCount()>0) {
					if(vyberSouboru.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
						String nazevSouboru = vyberSouboru.getSelectedFile().getAbsolutePath(); 
						if(!nazevSouboru.endsWith(".ev")) {
							nazevSouboru = nazevSouboru +".ev";
						}
						try {
							FileOutputStream fos = new FileOutputStream(nazevSouboru);
							PrintWriter pw = new PrintWriter(fos);
							for (Polozka polozka: polozky) {
								pw.println(polozka);
							}
							pw.close();
							fos.close();
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
					
				}else {
					zobrazChybu("��dn� data", "Nejd��ve vlo�te nebo na�t�te data do tabulky.");
				}
				
			}
			
		});
		
		model.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				if(e.getColumn()==3) {
					System.out.println("Zm�n�na hodnota "+ model.getValueAt(e.getLastRow(), e.getColumn()));
				}
				
			}
			
		});
		
		
		add(sever,BorderLayout.NORTH);
		add(stred, BorderLayout.CENTER);
		add(jih, BorderLayout.SOUTH);
		pack();
		setVisible(true);

	}
	
	public void zobrazChybu(final String nadpis ,final String zprava) {
		JOptionPane chyba = new JOptionPane();
		chyba.showMessageDialog(this, zprava,nadpis, chyba.ERROR_MESSAGE);
	}
	public double sectiCeny() {
	    double vysledek=0;
		for(Polozka polozka:polozky) {
		vysledek+=polozka.getCena();	
		}
		return vysledek;
	}
	
}
