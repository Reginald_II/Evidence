package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VyberRezimu extends JFrame{
	boolean vybranRezim;
	int rezim;
	
	
	public VyberRezimu() {
		super();
		setTitle("Volba re�imu");
		setSize(800, 600);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);;
		setLayout(new BorderLayout());
		this.vybranRezim=false;
		
		//Definice severu pro ot�zku
		
		JPanel sever = new JPanel();
		sever.setLayout(new BoxLayout(sever,BoxLayout.LINE_AXIS));
		sever.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JLabel otazka = new JLabel("Zvolte si re�im aplikace");
		sever.add(otazka);
		
		JPanel stred = new JPanel();
		stred.setLayout(new BoxLayout(stred,BoxLayout.LINE_AXIS));
		stred.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));		
		JButton tabulkovy = new JButton("Tabulkov�");
		
		tabulkovy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				vybranRezim =true;
				rezim = 1;
				
				
			}
		});
		
		JButton seznamovy = new JButton("Seznamov�");
		
		seznamovy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				vybranRezim =true;
				rezim = 2;
				
			}
		});
		
		stred.add(tabulkovy);
		stred.add(Box.createRigidArea(new Dimension(10, 0)));
		stred.add(seznamovy);
		stred.add(Box.createRigidArea(new Dimension(10, 0)));
		
		
		add(sever,BorderLayout.NORTH);
		add(stred,BorderLayout.CENTER);
		pack();
		setVisible(true);
		
		
	}


	public boolean isVybranRezim() {
		return vybranRezim;
	}


	public int getRezim() {
		return rezim;
	}
	

}
