package gui;

import java.awt.Component;
import java.awt.HeadlessException;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class VyberSouboru extends JFileChooser{
	public VyberSouboru() {
		super();
		setFileFilter(new FileFilter()				{
			public boolean accept(File f) {
				if(f.isDirectory() || f.getName().endsWith(".ev")) {
					return true;
				}else {
					return false;
				}
			}

			@Override
			public String getDescription() {
				return "Data evidence (.ev)";
			}
			
				}
				);
	}
	@Override
	public int showSaveDialog(Component parent) throws HeadlessException {
		setDialogTitle("Ulo�it");
		setFileSelectionMode(FILES_ONLY);
		setApproveButtonText("Ulo�it");
		return super.showSaveDialog(parent);
	}
	
	@Override
	public int showOpenDialog(Component parent) throws HeadlessException {
		setDialogTitle("Otev��t");
		setFileSelectionMode(FILES_ONLY);
		setApproveButtonText("Otev��t");
		return super.showOpenDialog(parent);
	}

}
