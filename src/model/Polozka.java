package model;

public class Polozka {

	private String nazev;
	private double cena;
	private String mena;

	public Polozka(String nazev, double cena, String mena) {
		this.nazev = nazev;
		this.cena = cena;
		this.mena = mena;
	}

	public String getNazev() {
		return nazev;
	}

	public double getCena() {
		return cena;
	}

	public String getMena() {
		return mena;
	}

	public void setNazev(String nazev) {
		this.nazev = nazev;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public void setMena(String mena) {
		this.mena = mena;
	}

	@Override
	public	String toString() {
	return	this.nazev+" "+this.cena+" "+this.mena;
}
}	
